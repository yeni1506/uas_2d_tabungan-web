<?php
    $DB_NAME = "tabungan";
    $DB_USER = "root";
    $DB_PASS =  "";
    $DB_SERVER_LOC = "localhost";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $sql = "SELECT m.id_anggota,m.nama,p.setoran,concat('http://192.168.43.167/tabungan/images/',photos) as url, m.alamat , m.no_hp, j.kelamin
		FROM mahasiswa m,transaksi p , jenis_kelamin j
        WHERE m.id_setor=p.id_setor and m.id_jk = j.id_jk";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $data_mhs = array();
            while($mhs = mysqli_fetch_assoc($result)){
                array_push($data_mhs,$mhs);
            }
            echo json_encode($data_mhs);
        }
    }
?>