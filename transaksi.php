<?php
    $DB_NAME = "tabungan";
    $DB_USER = "root";
    $DB_PASS =  "";
    $DB_SERVER_LOC = "localhost";
    $data_mhs = array();
    if ($_SERVER['REQUEST_METHOD'] == 'GET') {
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $sql = "SELECT * FROM transaksi";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            while($mhs = mysqli_fetch_assoc($result)){
                array_push($data_mhs,$mhs);
            }
        }
    }
?>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tabungan</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-info bg-info text-white">
        <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link text-white" href="#">Home</a>
                <a class="nav-item nav-link active text-white" href="anggota.php">Data Tabungan <span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link text-white" href="transaksi.php">List Setoran <span class="sr-only"></span></a>
                <a class="nav-item nav-link text-white" href="about.php">About</a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
    <div class="container">
    <h4 class="mt-3 mb-3">Setoran</h4>
    <table class="table">
    <thead>
        <tr>
        <th scope="col">ID</th>
        <th scope="col">Setoran</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($data_mhs as $mhs) : ?>
        <tr>
            <td><?= $mhs['id_setor'] ?></td>
            <td><?= $mhs['setoran'] ?></td>
        </tr>
    <?php endforeach ?>
    </tbody>
    </table>
</div>
<script src="js/bootstrap.min.js"></script>
</body>
</html>